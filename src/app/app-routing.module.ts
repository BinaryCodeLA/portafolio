import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntroComponent } from './pages/intro/intro.component';
import { AboutComponent } from './pages/about/about.component';
import { ContactComponent } from './pages/contact/contact.component';
import { EducationComponent } from './pages/education/education.component';
import { ProjectsComponent } from './pages/projects/projects.component';
import { ReferenceComponent } from './pages/reference/reference.component';
import { SkillsComponent } from './pages/skills/skills.component';

const routes: Routes = [
 {
    path:'',
    component:IntroComponent
  },
  {
    path:'home',
    component:IntroComponent
  },
  {
  	path: 'about',
  	component : AboutComponent
  },
  {
    path: 'contact',
    component : ContactComponent
  },
  {
    path: 'education',
    component : EducationComponent
  },
  {
    path: 'project',
    component : ProjectsComponent
  },
  {
    path: 'reference',
    component : ReferenceComponent
  },
  {
    path: 'skill',
    component : SkillsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
