import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarMenuComponent } from './components/navbar-menu/navbar-menu.component';
import { FooterComponent } from './components/footer/footer.component';

import { IntroComponent } from './pages/intro/intro.component';
import { AboutComponent } from './pages/about/about.component';
import { ContactComponent } from './pages/contact/contact.component';
import { EducationComponent } from './pages/education/education.component';
import { ProjectsComponent } from './pages/projects/projects.component';
import { ReferenceComponent } from './pages/reference/reference.component';
import { SkillsComponent } from './pages/skills/skills.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatSliderModule } from '@angular/material/slider';
import { MatButtonModule } from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    NavbarMenuComponent,
    AboutComponent,
    ContactComponent,
    EducationComponent,
    FooterComponent,
    IntroComponent,
    ProjectsComponent,
    ReferenceComponent,
    SkillsComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatButtonModule,
    MatMenuModule
      ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
